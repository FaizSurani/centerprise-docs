# XML File Destination

<iframe width="560" height="315" src="https://www.youtube.com/embed/qPFBS8zcC2g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Adding an XML/JSON file destination object allows you to write to an XML or a JSON file. An example of what an XML/JSON file destination object looks like is shown below.

 ![](xml-file-destination-images/mceclip0.png)

To configure the properties of an **XML/JSON Destination Object** after it was added to the dataflow, right-click on it and select **Properties** from the context menu. The following properties are available:

 

**General Properties screen:**

**File Path** – specifies the location of the destination XML file. Using UNC paths is recommended if running the dataflow on a server.

**Note**: To open an existing destination file for editing in a new tab, click XXX icon next to the File Path input, and select Edit File.

 

**File Options:**

Using the Encoding dropdown, select the appropriate encoding scheme for your destination file.

Check “Format XML Output” checkbox to have line breaks inserted into the destination XML file for improved readability.

 

**Schema Options:**

**Read From Schema File** – specifies the location of the XSD file controlling the layout of the XML destination file.

**Note**: You can generate the schema based on the content of the destination XML file if the file already exists.  The data types will be assigned based on the destination file’s content. Note that the existing destination file will be overwritten when the dataflow runs.

To generate the schema, click the icon next to the Schema File input, and select Generate.

To edit an existing schema, click the icon next to the Schema File input, and select Edit File. The schema will open for editing in a new tab.

 

Using the Root Element dropdown, select the node that should be the root of your destination schema. Any nodes up the tree will be excluded.

**Note**: To ensure that your dataflow is runnable on a remote server, please avoid using local paths for the destination. Using UNC paths is recommended.

 

**Destination XML Layout screen** shows the layout of your destination XML file.

 

**General Options screen:**

This screen shares the options common to most objects on the dataflow. For more information on general options, see [General Options](https://astera.zendesk.com/entries/20769283#General_Options).