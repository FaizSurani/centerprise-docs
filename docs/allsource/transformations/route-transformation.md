# Route Transformation

A Route transformation invokes one or more paths in the dataflow, in accordance with some decision logic expressed as a set of Rules. Using Route transformation, you can create some custom logic involving multiple paths and adapt it to suit your data processing scenario.

For example, a record passing some rule will be written into Destination1. A record passing another rule will be written to Destination2. A record passing still other rule will not go anywhere, and any remaining records will be fed to a downstream transformation object.

<iframe width="560" height="315" src="https://www.youtube.com/embed/CyerOiGzCcs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
## Steps to Use the Route Transformation in Centerprise 

To add a Route transformation, drag the Route object from the Transformations group in the Flow toolbox and drop it on the dataflow.

![](route-transformation-images/mceclip0.png)

An example of what a Route object might look like is shown below:

 ![](route-transformation-images/mceclip1.png)

 

To configure the properties of a Route object after it was added to the dataflow, right-click on it and select Properties from the context menu. The following properties are available:

**Layout Builder screen:**

The Layout Builder screen allows you to add or remove fields in the field layout, as well as select their data type. The fields added in Layout Builder will show in the Input node inside the object box, as well as in all Rule nodes corresponding to the number of rules created (see below).

![](route-transformation-images/mceclip3.png)

 

**Route Transformation Properties screen:**

Router Transformation Properties screen provides the interface to manage the Router rules.

![](route-transformation-images/mceclip4.png)

Click the![image47.gif](route-transformation-images/image47.gif)icon  to create a new Router rule.

Type a descriptive name for the rule in the Description field.

In the Expression input, enter the expression making the rule. For example, LTV > 60 and LTV <= 80, or any rule or condition you want to apply to your data. Alternatively, you can click on the ![](route-transformation-images/mceclip6.png) button to open Expression Builder -- a tool that allows you to visually build your rule using Record tree and IntelliSense.

Click Compile to check for any syntax errors in your rule. The Status should read “Successful" for a successful compilation.

To activate the rule, check the Active switch.

Add other Router rules if necessary. To delete an existing Router rule, select it and click the ![image48.gif](route-transformation-images/image48.gif)icon.

Note: Each Router rule you add here will have its own Rule node in the Router object box once you close the Properties dialog. You can then map it to downstream objects on the dataflow as needed.

Note: Any Route transformation object has the Default node.  This node handles records not passing any rules defined in your Route transformation object.

**General Options screen:**

This screen shares the options common to most objects on the dataflow.

Clear Incoming Record Messages

When this option is on, any messages coming in from objects preceding the current object will be cleared. This is useful when you need to capture record messages in the log generated by the current object and filter out any record messages generated earlier in the dataflow.

Do Not Process Records with Errors

When this option is on, records with errors will not be output by the object. When this option is off, records with errors will be output by the object, and a record message will be attached to the record. This record message can then feed into downstream objects on the dataflow, for example a destination file that will capture record messages, or a log that will capture the messages and as well as collect their statistics.

The Comments input allows you to enter comments associated with this object.

 

## Usage

An example of using the Route transformation object is shown below.

![](route-transformation-images/mceclip2.png)