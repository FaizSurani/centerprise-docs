# Apply To All Transformation

The ApplyToAll Transformation applies an [Expression Transformation](https://astera.zendesk.com/entries/21789027-Expression-Transformation) to all mapped elements. This transformation is useful when applying a common [Expression Transformation](https://astera.zendesk.com/entries/21789027-Expression-Transformation) to all data without the need to build multiple Expression Transformations.

Related:[ Expression Transformation](https://astera.zendesk.com/entries/21789027-Expression-Transformation)

 

## Steps to Use the ApplyToAll Transformation 

To use an ApplyToAll transformation, drag and drop the **ApplyToAll** from the Transformations section in the toolbox to the flow designer.

![](apply-to-all-transformation-images/mceclip0.png)

 

Map all the data to the ApplyToAll transformation created on the flow designer. These are all the data that a common expression will be applied to.

The properties window is also where the expression to be applied is specified. A parameter called **$FieldValue** will be available for an expression to be applied to. This will represent all of the data that was mapped to the ApplyToAll transformation.

## Sample

In this example, we are going to apply an Expression transformation to two string fields to convert them into lower case values.

 

![](apply-to-all-transformation-images/mceclip2.png)

 

The properties for *ApplyToAl*l will give you the option to add an expression to a *$FieldValue* parameter. This will apply the expression to all of the mapped elements on the transformation.

![](apply-to-all-transformation-images/mceclip1.png) 

In this example, the ApplyToAll transformation takes the MaritalStatus and Gender field parameters and transforms them to lower case values.