# Expression Transformation

Expression Transformation defines an expression that can be used to process the incoming value (or values) according to the expression's logic. Expression Transformation can also provide values which do not depend on any user-provided input data and also modify and use variables for various calculations.

Expression Transformation uses an expression which is the logic for transforming data. Expressions can use functions and operations to fields to manipulate data. There are a lot of built-in functions for expressions to use such as string manipulations, data conversions, date and time manipulations, etc. Operations such as calculation, comparision, assignments, etc, are also available. The list of functions can be found on the Expression builder [(Click here for more information about the Expression Builder)](https://astera.zendesk.com/entries/82084419-Expression-Builder). Expressions uses the Astera Rules Language.

## How to use Expression Transformation

1\. To add an Expression transformation, drag the *Expression* object from the Transformations group in the *Toolbox* and drop it on the dataflow.

![Expression6.png](expression-transformation-images/Expression6.png)

An example of what an Expression transformation object might look like is shown below.

![](expression-transformation-images/mceclip0.png)

To configure the properties of an Expression object after it was added to the dataflow, right-click on it and select Properties from the context menu. The following properties are available:

**Layout Builder screen:**

Fields can be added to the Layout Builder and can be configured by the following:

- Name - The name of the Field
- Data Type - The type of data the field is. Different expressions can be made based on what the data type of the field is.
- Input - The field can be mapped as an input.
- Output - The field can be mapped as output. If an expression is present, the expression is applied to the output.
- Variable - Turns the field into a variable which can then be used on other fields. These expressions are calculated first and then assigned to other expressions that use it, no matter what the ordering of the fields are. Once a field turns into a variable, it cannot be configured for input and output mapping.
- Expression - The expression used for the modifying the field or fields.

![](expression-transformation-images/mceclip2.png)

The order of expression operations are as follows: Input --> Variable --> Output. They are also applied in a top-to-bottom order. It is recommended to use temporary fields as Variable fields to store data to be used in Output expressions.

Click on the Expression area of the field to add an expression. You can type in the expression directly, or use the Expression Builder by clicking on the button provided next to the field. (See the button displaying (...) in the screenshot below).

![Expression8.png](expression-transformation-images/Expression8.png)

**Expression Builder:**

The Expression Builder provides the interface to help you create your expression[(Click here for more information about the Expression Builder)](https://astera.zendesk.com/entries/82084419-Expression-Builder).

![](expression-transformation-images/mceclip3.png)

## Examples

### String Concatenation

The sample screenshots above shows an expression that builds an address string from the fields: Name, Street, State, and Zip. This example shows string concatenation with different string fields with the use of the plus(+) symbol. 

### Nesting Functions

Another use for the expression transformation is to combine two or more function transformation into one single expression transformation.

![](expression-transformation-images/mceclip4.png)

![](expression-transformation-images/mceclip5.png)

In this example, the Expression Transformation named "ProperCase" performs the same transformation as the two functions: ToLower and ToProper. They both convert the string into lower case and then performs a proper case transformation.

### Variable Fields

Fields can be converted into variables to be used in other fields.

![](expression-transformation-images/mceclip6.png)

![Expression10.png](expression-transformation-images/Expression10.png)

In this example, the *CurrentHour* field stores the value from the *Hour(Now())*function which takes the current hour of the current date. *IsHappyHour* then outputs true if the *CurrentHour* is between 5:00 pm and 6:00 pm. The advantage of having the variable property checked is that even when the order of the fields are swapped (e.g. *IsHappyHour* comes before *CurrentHour*), *CurrentHour* is still calculated first and assigned to the value that is used in *IsHappyHour*. 