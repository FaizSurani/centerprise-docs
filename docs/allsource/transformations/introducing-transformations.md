# Introducing Transformations

Transformations are used to perform a variety of operations on data as it moves through the dataflow pipeline. Centerprise provides a full complement of built-in transformation enabling you to build sophisticated data maps.

Centerprise transformations are divided into two types—record level and set level. Record level transformations are used to create derived values by applying a lookup, function, or expression to fields from a single record. Set level transformations, on the other hand, operate on a group of records and may result in joining, reordering, elimination, or aggregation of records. Example of record level transformations include lookups, expression, and function transformations while set transformations include join, sort, route, distinct, etc. Data sources and destinations are also considered set transformations. In Centerprise, data records flow between set transformations. Record level transformations are used to transform or augment individual fields during these movements.

A record level transformation can be connected to only one set transformation. For instance, a lookup or expression cannot receive input from two different set transformations.

Other than transformations that enable combining multiple data sources—such as join, merge, and union—transformations can receive input from only one set transformation. Transformations, however, can receive input from any number of record level transformations as long as all these record level transformations receive input from the same transformation.  