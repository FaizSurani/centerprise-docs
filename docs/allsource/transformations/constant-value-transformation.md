# Constant Value Transformation

Constant Value Transformation returns a single, predefined value for all records.

 

## Steps

To add a Constant Value transformation, drag the Constant Value object from the transformations section in the toolbox and drop it on the dataflow.

![](constant-value-transformation-images/mceclip0.png)

An example of what a Constant Value object might look like is shown below.

![](constant-value-transformation-images/mceclip1.png)

To configure the properties of a Constant Value object after it was added to the dataflow, right-click on it and select Properties from the context menu. The following properties are available:

**Constant Value Properties screen**

*Constant Value:* Enter the value that will be returned by this object.

**General Options screen**

This screen shares the options common to most objects on the dataflow.

 

## Usage

An example of a dataflow using Constant Value objects is shown below:

![](constant-value-transformation-images/mceclip2.png)