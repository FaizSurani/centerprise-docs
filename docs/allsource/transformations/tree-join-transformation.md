# Tree Join Transformation

Tree Join Transformation enables creation of complex tree structures such as EDI documents or complex XML documents. Unlike relational join which combines left and right elements to create a new record, tree join enables creation of collection and member nodes.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xUmcpt-7P50" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

An example of a Tree Join transformation in a dataflow is shown below.

![](tree-join-transformation-images/mceclip0.png)