# Passthru Transformation

A Passthru Transformation creates a new dataset based on the elements that were passed to the transformation. This is useful for organizing datasets for better readability and grouping of values that are otherwise calculated over and over again (e.g. a [Sequence Generator Transformation)](https://astera.zendesk.com/entries/21435108-Sequence-Generator).

## Usage

To use an Passthru transformation, drag and drop the **Passthru** Transformation from the **Transform** Group in the Flow toolbox to the flow designer.

![Passthru1.png](passthru-transformation-images/Passthru1.png)

This is what a Passthru transformation object looks like:

![](passthru-transformation-images/mceclip0.png)

Map all the data to the Passthru transformation created on the flow designer. These will be the data for the new dataset.

The properties window has the option of adding new member objects, or Collection Objects. This is where the dataset can be organized based on what is needed.

![](passthru-transformation-images/mceclip4.png)

## Example

![](passthru-transformation-images/mceclip5.png)

In this example, we are creating a new dataset from:

- the EmployeeID, FirstName, LastName, Extension, and ReportsTo from an excel source file
- a Variable called RoomID
- a Sequence Generator which will generate the CubicleID

and mapping it to two an Excel and a Database destination files.

The Passthru Transformation gives a nice option of grouping the data needed for other tasks and storing any calculated data for each element of the dataset. In this example, the Sequence Generator is able to assign a unique CubicleID for each employee, and still retain the same value on both the excel destinations. 