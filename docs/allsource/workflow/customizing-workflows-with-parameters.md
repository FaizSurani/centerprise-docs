# Customizing Workflows with Parameters

The workflows you create with Centerprise can be ported to any number of target environments that may use different connections to data providers, or other settings specific to the target environment. For example, you can create and test your workflow in a staging environment, and then deploy it in your production environment. Prior to deploying to your target environment, you may need to update the workflow’s data connections making them appropriate for the target environment. The updating of data connections is made easy by using the Parameter Information Replacement dialog.

To open the Parameter Information Replacement dialog, click the![image49.gif](customizing-workflows-with-parameters-images/image49.gif)icon on the Workflow toolbar.

The left-hand pane of the Parameter Information Replacement dialog shows the parameter tree with all the objects present in the workflow. Select an object in the tree, and update its connection in the right-hand pane’s input as necessary.

Objects in the tree can be grouped by category, such as Source, Workflow Tasks etc, or left ungrouped. The grouping is done by means of the two icons ![image49.gif](customizing-workflows-with-parameters-images/image49.gif) ![image49.gif](customizing-workflows-with-parameters-images/image49.gif)in the top left of the dialog.

Note: In the case of a scheduled workflow running on a server, a similar functionality is available in the Job Parameters tab of the scheduled job interface in the Scheduler. For more information on Scheduler, see Scheduling Jobs.

## Context Information

In addition to aligning your data connections with a target environment, you might also want to ensure that any context information is suitable for the environment that your workflow will run in. For example, make sure that the server name variable points to the correct server instead of being hard-coded. This can be achieved by using Context Information parameters. Context Information parameters take their values dynamically at workflow run-time. These parameters include ServerName, JobId, UserName, DroppedFilePath, among others.

To use context information parameters in your workflow, drag-and-drop the Context Information object from the Resources group in the Flow toolbox on the workflow.

An example of what a Context Information object might look like is shown below.

 ![](customizing-workflows-with-parameters-images/mceclip0.png)

The following parameters are available:

Name - name of the workflow, for example workflow1155.wf

JobId - ID of the job assigned by the server

ScheduledJobId - ID of the job associated with a schedule in case of scheduled running on a server

ServerName - name of the server running the workflow

UserName - user account that runs the workflow

DroppedFilePath - path to the file being dropped, in case of a scheduled job of type 'when file is dropped' 

Note: You can also assign default values to parameters. These values will take effect when no other value is available. To select a default value, open the properties of the Context Information object and enter the default value next to the parameter.

## Custom Parameters

You can also define any number of custom parameters. These custom parameters can be used throughout the workflow. They can be initially assigned a default value, and then their value can change in the course of running your workflow.

Custom parameters can be configured in the Variables object. To add a Variables object in a workflow, drag it from the Resources group in the Flow toolbox and drop it on the workflow.

An example of what a Variables object looks like is shown below.

 ![](customizing-workflows-with-parameters-images/mceclip0-1563490829359.png)

To add a new parameter, open the Variables object properties and enter your new parameter in the grid.  The parameter should be assigned a data type, and it can be optionally assigned a default value.

You can also refer to most objects’ property values by using the $ (dollar-sign) notation, such as $object_name.field_name. This functionality is available in any area in the workflow that accepts $ parameters.

Examples of workflow areas that accept $ parameters are:

- Run SQL Script task
- Parameter Information Replacement dialog
- Send Mail task

Below is an example of using a $ parameter in a Run SQL Script object:

SELECT [dbo].[Customers].[CustomerID],

[dbo].[Customers].[CompanyName],

[dbo].[Customers].[ContactName],

[dbo].[Customers].[ContactTitle]

FROM [dbo].[Customers]

where CustomerId = '$(SQLQuerySource1.CustomerID)'