# Using Workflow Designer

## Adding Objects

A workflow always has one or more workflow tasks making up the core of the workflow, and may also have source objects, destinations objects, transformation objects or resource objects in the same workflow design.

<iframe width="420" height="315" src="https://www.youtube.com/embed/heghEA44_X0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Depending on the type of object, an object can be added to the workflow in one of the following ways:

For Flat File Sources:

1\. Using Drag-and-Drop.  You can drag a file of the type listed below from an Explorer window and drop it onto an open workflow tab in Centerprise.  

- Excel
- Delimited
- Fixed-Length File

The advantage of using drag-and-drop compared to other methods is that many of the object’s properties are pre-populated for you based on the file’s content. For example, the field layout is automatically filled out so that there is no need to manually create it.

Note: XML files cannot be added to a dataflow using the drag-and-drop method.

2\. Using Toolbox.  You can add a source object by selecting it from the appropriate category in the Flow toolbox.

For example, to add a source comma-delimited file object, expand the Sources group in the Flow toolbox, and the drag-and-drop the Delimited File Source tool on the workflow.

Note that an object added this way initially does not have any properties defined.  To define its properties, double click on the object title, or right-click and select Properties from the context menu.

In the Properties screen that opens, select the File Path of the file that will be associated with the object.  Field layout and other properties can then be populated based on the file’s content.

3\. Copying and pasting an existing object from the same or different workflow or dataflow.  If your source is already defined in the same or different workflow (or dataflow), you can copy the existing object and paste it into your workflow. The object being copied retains the properties of the original object, and is assigned a unique new name to distinguish it from the original object.

For XML Sources:

1\. Using Toolbox. To add an XML source to the workflow, use the XML File Source tool in the Sources group in the Flow toolbox.

Note that the XML File object initially will not have any properties defined.  To define its properties, double click on the object’s title, or right-click and select Properties from the context menu.

In the Properties screen that opens, select the File Path of the XML file that will be associated with the object.  Additionally, please provide the path to the XSD schema that controls the layout of your XML file.

As with flat files, you can copy and paste an existing XML object from the same or different workflow or dataflow.  The object being copied retains the properties of the original object, and is assigned a unique new name to distinguish it from the original object.

For Database Sources:

1\. Using drag-and-drop.  You can drag a database table or view from the Data Source Browser and drop it on an open workflow tab.  

To open the Data Source Browser, go to View -> Data Source Browser. Connect to the appropriate server, then expand the Database tree and expand Tables (or Views) tree to select your table (or view).  Drag-and-drop the selected table or view to the workflow.

By default, the database table is added as a Database Table Source object.

To add a data model source, press and hold 'Ctrl' key while dragging and dropping a table (or view) from the Data Source Browser.

As with files, you can copy and paste an existing database table object from the same or different workflow or dataflow.  The object being copied retains the properties of the original object, and is assigned a unique new name to distinguish it from the original object.

For Any Other Types of Objects  (for example, workflow tasks or resources objects):

1\. Using Toolbox.  You can add an object by selecting it from the appropriate category in the Flow toolbox.

Note that an object added this way initially does not have any properties defined.  To define its properties, double click on the object title, or right-click and select Properties from the context menu.

2\. Copying and pasting an existing object from the same or different workflow or dataflow.  If your object is already defined in the same or different workflow (or dataflow), you can copy the existing object and paste it into your workflow. The object being copied retains the properties of the original object, and is assigned a unique new name to distinguish it from the original object.