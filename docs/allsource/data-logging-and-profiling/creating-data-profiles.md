# Creating Data Profiles

## Data Profile

A Data Profile object gathers field statistics for an object to which the data profile is linked. The statistics are collected for each of the selected fields at the time the dataflow runs. They are stored in a proprietary XML file with the .prof extension. This file can be opened for viewing in a new tab when you double-click on its link. An example of a field statistics screen is shown below.

![](creating-data-profiles-images/mceclip0.png)

 

To add a data profile, drag the Data Profile object from the Logs and Profiles group in the Flow toolbox and drop it on the dataflow.

An example of what a Data Profile object might look like is shown below.

![](creating-data-profiles-images/mceclip1.png)

 

To configure the properties of a Data Profile object, right-click on it and select Properties. The following properties are available:

Profile File: specifies the path to the profiler file storing field statistics.

Field Statistics dropdown allows you to select the detail level of statistics to collect. Select among the following detail levels:

- Basic Statistics: This is the default mode. It captures the most common statistical measures for the field’s data type.
- No Statistics: No statistics is captured by the Data Profile.  
- Detailed Statistics – Case Sensitive Comparison. Additional statistical measures are captured by the Data Profile, for example Mean, Mode, Median etc, using case-sensitive comparison for strings.
- Detailed Statistics – Case Insensitive Comparison. Additional statistics are captured by the Data Profile, using case insensitive comparison for strings.

Note: A Data Profile object is designed to capture statistics for an entire field layout. For this reason, it should be linked to the main Output port of the object whose field statistics you wish to collect.

General Options screen

The Comments input allows you to enter comments associated with this object.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9_Dj3Qxp27Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
## Usage

An example of a dataflow with a Data Profile is shown below:

![](creating-data-profiles-images/mceclip2.png)