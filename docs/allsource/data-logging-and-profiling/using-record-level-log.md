# Using Record Level Log

## Record Level Log

Using a record level log in a dataflow is similar to using the data profiler in a transfer setting. A record level log captures the status of each record transferred, such as Success, Error, Warning, or Skip, and includes snapshots of the source record and the destination record, as well as provides additional details, such as error messages.

You can have any number of record level logs on the dataflow; each record level log will collect the status of the records in the object that the record level log is connected to.

 

## Steps

A record level log is saved to a .prof file, which by default is stored in the Temp folder of the user profile.

To display a record level log, click on the log link in Transfer Progress window. Or you can double click on a .prof file in Windows Explorer.

The Record Level Detail tab in the record level log displays successes, errors, and warnings down at the record level. Using Record Level Filter, available via the  ![image50.gif](using-record-level-log-images/image50.gif) dropdown, you can view a subset of records meeting your search criteria, such as All, Success, Errors, Skipped, Warnings, or Errors and Warnings.

Note: While viewing a record level log, you can optionally export source records into a delimited file or Excel file, or you can export the profile itself into an Excel file, all by using the  ![image51.gif](using-record-level-log-images/image51.gif) dropdown on the Profile toolbar.

To add a record level log, drag the Record Level Log object from the Logs and Profiles group in the Flow toolbox and drop it on the dataflow.

An example of what a Record Level Log object might look like is shown below.

![](using-record-level-log-images/mceclip0.png)

To configure the properties of a Record Level Log object, right-click on it and select Properties. The following properties are available:

Profile File: specifies the path to the profile file that stores the Record Level Log.

Log Level Type – select among the following types of events to log:

- All – all records (including Success records) are logged
- Errors – only error records are logged
- Warnings – only warning records are logged
- Errors and Warnings – both error and warning records are logged
- Off – no logging.

Stop Logging After … Records with Errors – allows you to limit excessive logging by setting a cap on the maximum number of errors to be logged. The logging stops after the cap has been reached.

The default value is 1000 errors.

General Options screen

The Comments input allows you to enter comments associated with this object.

 

## Usage

An example of a dataflow with a Record Level Log object is shown below:

![](using-record-level-log-images/mceclip1.png)

 

An example of a what a record level log could look like is shown below:

![](using-record-level-log-images/mceclip0.png)