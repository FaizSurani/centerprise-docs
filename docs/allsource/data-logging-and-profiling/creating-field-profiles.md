# Creating Field Profiles

Field Profile captures statistics for selected fields from one or several objects. Field Profile is essentially a transformation object as it provides Input and Output ports similar to other transformations. The Output ports make it possible to feed the statistics collected to another object on the dataflow, for example, a destination database table, which can then be used to analyze the collected data.

This is what a Field Profile object looks like: 

![](creating-field-profiles-images/mceclip2.png)

An example of a dataflow fragment with a Field Profile in it is shown below.

![](creating-field-profiles-images/mceclip0.png)

Statistics will be collected only for the fields linked to the Input port of the Field Profile object. This way, you can selectively collect statistics for a subset of fields from the selected field layout.

Note: To preview field statistics, right-click on a Field Profile object and select Preview Data.

To add a field profile, drag the Field Profile object from the Logs and Profiles group in the Flow toolbox and drop it on the dataflow.

To configure the properties of a Field Profile object, right-click on it and select Properties. The following properties are available:

Meta Object Builder screen is used to add or remove fields in the field layout, as well as select their data type.

Note: To quickly add fields to the layout, drag and drop the node Output port of the object whose layout you wish to replicate into the node Input port of the Field Profile object. The fields added this way show in the list of fields inside the Input node and as well as in the Meta Object Builder.

Field Profile Properties screen:

Case Sensitive. Turn this option on if you need to collect statistics using case sensitive comparison for strings.

 

## Usage

In the example below, field profile object collects statistics for LoanID, Rate, and Amortization_Type fields and saves it to an Excel spreadsheet.

![](creating-field-profiles-images/mceclip1.png)