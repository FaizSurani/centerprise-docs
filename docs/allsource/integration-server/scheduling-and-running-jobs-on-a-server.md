# Scheduling and Running Jobs on a Server

<iframe width="560" height="315" src="https://www.youtube.com/embed/wy0c9_EQfTA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Scheduling Jobs

Centerprise server comes with the built-in Scheduler, which allows you to run your jobs once, or repetitively according to selected schedule.  Among the available schedules are: ”Run Once”, ”Hourly”, ”Daily”, ”Weekly”, ”Monthly”, and ”When file is dropped”. Once you create a schedule, the job associated with that schedule will run according to the schedule.  

Scheduled jobs run silently in the background. A new tab in the Transfer Progress pane will be added for each scheduled job that has started running. Results of the scheduled jobs are stored in the Transfer Log in the same way as for regular jobs.

Optionally, you can set an email notification to signal the start and/or the completion of a scheduled job. An additional email can notify the selected recipient of any errors that occurred during the job.

To open Scheduler, click the![image3.gif](scheduling-and-running-jobs-on-a-server-images/image3.gif)icon on the Toolbar.

 

## Schedule Types

"Run Once” schedule allows you to specify a date and time in the future when a job should run.  Jobs on this schedule run only once.

”Hourly”, ”Daily”, ”Weekly”, and ”Monthly” schedules allow you to specify recurring jobs with the hourly, daily, weekly, or monthly frequency. Within the weekly and monthly frequencies, you can set the day that the job will run, such as every month on the 5th or every Tuesday. Each frequency can also be set for a specific time.

”When file is dropped” schedule runs the selected transfer setting when a file is dropped into a designated folder (called ”Watched” folder). You can optionally set the dropped file as the source file for the scheduled transfer setting. In that case, any source file provided in the transfer definition will be ignored; the dropped file becomes the source for the scheduled transfer.  In addition, you can create a custom path for the destination file, overriding the selection of the destination file in the transfer definition.

 

Steps:

1\.       Click the **Schedule** icon ![image3.gif](https://astera.zendesk.com/attachments/token/ugcjighbfkvzjf3/?name=image3.gif)on the Toolbar.

2\.       From the Scheduler window menu bar, click the **Add Scheduler Task** icon![image141.jpg](scheduling-and-running-jobs-on-a-server-images/image141.jpg)**.** A new task is added. You can schedule multiple jobs to run by using this step.

3\.       In the **File Path** box, specify the storage location of the job you want to schedule.

OR

Click![image143.jpg](scheduling-and-running-jobs-on-a-server-images/image143.jpg) to browse to the storage path of the job.

 

4\.       From the **Frequency** list, select how frequently do you want the scheduled task to run.

Note:    Depending on the frequency you choose, other options will be provided. For example, if you choose to run the scheduled task on a Weekly basis, you will be able to specify on what day of the week, and at what time you want to run the scheduled task.

The task has now been successfully scheduled.

You can click the **Run Task Now** button![image145.jpg](scheduling-and-running-jobs-on-a-server-images/image145.jpg) on the Scheduler menu bar, to run a selected scheduled task instantly.

You can also click the **Remove Selected Task** button![image146.jpg](scheduling-and-running-jobs-on-a-server-images/image146.jpg) on the Scheduler menu bar, to delete a selected scheduled task.

After a scheduled job has run, the details are displayed in Server Jobs tab**.**

 

## Advanced Options

For scheduled tasks of type “when a file is dropped”, you can specify what should happen with the source file once the scheduled task finished running.

Available actions are:

No action—the source file is not changed in any way.

Rename source file - append job ID  –   The source file is renamed by appending the scheduled task’s job ID to the file name. For example:

Rename source file - append datetime –  The source file is renamed by appending the start date and time of the current task to the file name, for example:

Move file to – the source file is moved to a selected folder.

Delete source file – the source file is deleted.

All of the above actions occur after a successful completion of the scheduled task.

Note: In the case of a scheduled task that runs a batch (as opposed to a single transfer), the above actions apply to the first transfer in the batch, as well as any subsequent transfers in which the source file path is the same as in the first transfer.

Similar to the scheduled transfers, the file actions for batches are completed only after the batch has finished running.

 

## Email Notification

Using Notification Email tab, you can configure emails to be sent when a scheduled task was completed.  An email can also be sent to notify you that a task has started and whether it has finished successfully or failed.

Depending on the level of information you need in the notification email, you can select between two modes:  Summary and Details.

You can optionally include attachments to the email notification.  Such attachments can include source file, destination file, rejected records file or the Profiler file.

## Running Jobs on a Server

Many editions of Centerprise come with the built-in execution engine. This engine shows as Development server in the Server Explorer and in the Servers menu on the main Toolbar.

By default, any job such as dataflow, workflow, transfer setting, batch, or tables copy batch runs locally on the workstation that hosts Centerprise client.  

You can also run your job using a remote server. The advantage of running jobs on a dedicated server is better overall performance, as well as the ability to run many concurrent transfers without performance degradation.

To run a job locally, select Development in the Servers menu on the main Toolbar. When you click the Run icon ![image12.gif](scheduling-and-running-jobs-on-a-server-images/image12.gif), your job will be executed on the built-in server. To run a job using a remote server, select the appropriate remote server in the Servers dropdown on the main Toolbar. When you click the Run icon![image12.gif](scheduling-and-running-jobs-on-a-server-images/image12.gif), your job will be executed by the server you selected.

 

Note: Centerprise Data Integrator Server must be installed on the target server, in order for you to run your jobs using that server.