.. RTD-Sphinx documentation master file, created by
   sphinx-quickstart on Fri Jul  5 12:15:22 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Centerprise's documentation!
=======================================

.. toctree::
   :maxdepth: 1
   :caption: Getting Started
   :name: getting-started-toc  
   
   getting-started/centerprise-8-whats-new-improved-fixed
   getting-started/known-issues-in-centerprise-8-beta
   getting-started/connecting-to-a-centerprise-server-using-lean-client   
   getting-started/how-to-build-a-cluster-database-and-create-repository
   getting-started/how-to-configure-the-server
   getting-started/how-to-connect-to-a-different-centerprise-server-from-the-lean-client
   getting-started/how-to-install-centerprise-lean-client
   getting-started/how-to-set-up-a-server-certificate-.pfx-file-in-a-new-environment
   getting-started/installing-client-and-server-applications
   
.. toctree::
   :maxdepth: 1
   :caption: Transformations
   :name: transformations-toc
   
   transformations/introducing-transformations
   transformations/aggregate-transformation
   transformations/apply-to-all-transformation
   transformations/constant-value-transformation
   transformations/denormalize-transformation
   transformations/distinct-transformation
   transformations/expression-transformation
   transformations/filter-transformation
   transformations/join-transformation
   transformations/list-lookup-transformation
   transformations/merge-transformation
   transformations/normalize-transformation
   transformations/passthru-transformation
   transformations/reconcile-transformation
   transformations/route-transformation
   transformations/sequence-generator
   transformations/sort-transformation
   transformations/subflow-transformation
   transformations/switch-transformation
   transformations/tree-join-transformation
   transformations/union-transformation
   
.. toctree::
   :maxdepth: 1
   :caption: Sources
   :name: sources-toc
   
   sources/setting-up-sources
   sources/cobol-file-source
   sources/database-table-source
   sources/delimited-file-source
   sources/excel-file-source
   sources/file-system-entries-source
   sources/fixed-length-file-source
   sources/sql-query-source
   sources/xmljson-file-source      
   
.. toctree::
   :maxdepth: 1
   :caption: Destinations
   :name: destinations-toc
   
   destinations/setting-up-destinations
   destinations/database-table-destination
   destinations/delimited-file-destination
   destinations/excel-file-destination
   destinations/fixed-length-file-destination
   destinations/sql-statement-destination
   destinations/xml-file-destination

.. toctree::
   :maxdepth: 1
   :caption: Data Logging and Profiling
   :name: data-logging-and-profiling-toc
   
   data-logging-and-profiling/creating-data-profiles
   data-logging-and-profiling/creating-field-profiles
   data-logging-and-profiling/using-data-quality-mode
   data-logging-and-profiling/using-data-quality-rules
   data-logging-and-profiling/using-record-level-log
   
.. toctree::
   :maxdepth: 1
   :caption: Workflow
   :name: workflow-toc
   
   workflow/adding-workflow-tasks
   workflow/creating-workflow-tasks
   workflow/creating-workflows
   workflow/customizing-workflows-with-parameters
   workflow/using-data-source-browser
   workflow/using-workflow-designer

.. toctree::
   :maxdepth: 1
   :caption: Subflow
   :name: subflow-toc
   
   subflows/using-subflows   

.. toctree::
   :maxdepth: 1
   :caption: Database Write Strategies
   :name: database-write-strategies-toc

   database-write-strategies/database-write-strategies
   database-write-strategies/source-diff-processor
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
