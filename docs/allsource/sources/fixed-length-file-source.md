# Fixed Length File Source

The Fixed-Length File Source in Centerprise provides a high-speed reader for files containing fixed length records. Fixed-Length File Source supports records with delimiters as well as without delimiters. Key features are discussed in the following subsections:

## Encodings and Character Sets

Centerprise supports a wide range of encoding and character sets including double byte and multi-byte character sets.

## Data Formats

With Centerprise, you can parse dates, numbers, and Boolean values in virtually any formats. Centerprise comes with a set of built-in formats for dates, numbers, and Boolean values and provides the ability to define additional formats.

## Filtering

Often, files contain rows that must be skipped. With Centerprise, you can specify criteria for rows that are skipped before parsing. This way, if the file contains multiple types of rows, Centerprise processes only the rows that meet your criteria.

## Hierarchical Files

Businesses often exchange complex transactions in the form of hierarchical fixed-length files. Centerprise Fixed-Length File Source provides support for hierarchical files. You can define hierarchical file layout and process the data file as a hierarchical file. Centerprise IDE provides extensive user interface capabilities for processing hierarchical structures.

## Layout Building

Centerprise Fixed-Length File Source features a visual layout builder that facilitates building of fixed length layout by visually marking start positions. Often, layout specifications are defined in Excel files that contain field names, data types, start positions, and lengths. You can import these specifications in Centerprise and quickly build fixed-length layout using these specifications.

## COBOL Files

If you are working with COBOL files and do not have COBOL copybooks, you can still import this data by visually marking fields in the layout builder and specifying field data types. For more advanced parsing of COBOL files, you can use Centerprise COBOL File Source.

 

## Steps

Adding a fixed-length file source object allows you to transfer data from a fixed-length file. An example of what a fixed-length file source object looks like is shown below.

![](fixed-length-file-source-images/mceclip0.png)

 

To configure the properties of a Fixed-Length Source object after it was added to the dataflow, right-click on it and select Properties from the context menu.