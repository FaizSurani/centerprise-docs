# Excel File Source

Centerprise Excel File Source provides a high-speed reader for Excel files. Excel File Source supports both .xls and .xlsx files. You can start reading from any location within an Excel workbook. Excel File Source provides the following key features:

## Layout Discrepancies

In many instances, file formats are inconsistent with agreed upon layout. Centerprise provides the functionality to handle these inconsistencies. Inconsistencies include extra fields, out of sequence fields, inconsistent field headers.

## Automatic Layout Building

Centerprise provides the ability to build file layout automatically by reading the sample data file. This feature correctly determines data types most of the time. You can manually change data types in the layout grid.

## Steps

Adding an Excel file source object allows you to transfer data from an Excel file. An example of what an Excel file source object looks like is shown below.

![](excel-file-source-images/mceclip0.png)

To configure the properties of an Excel Source object after it was added to the dataflow, right-click on it and select Properties from the context menu.

<iframe width="560" height="315" src="https://www.youtube.com/embed/n5MQ4x3u3bs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

