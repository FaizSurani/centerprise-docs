# COBOL File Source

Centerprise COBOL File Source provides the functionality to read a COBOL file. Centerprise features a high-speed COBOL file reader capable of efficiently processing large COBOL file. COBOL File Source features include:

### Copybook Parsing

Centerprise features a built-in parser that reads COBOL copybook specifications and automatically builds layout. Where copybook is not available, you can treat the file as a standard fixed length file and visually define field markers and specify data types and numeric formats.

### REDEFINES, OCCURS, and OCCURS DEPENDING

COBOL File Source provides full support for REDEFINES, OCCURS, and OCCURS DEPENDING. For REDEFINES, you can specify values that will be used to identify individual record types. These values can be defined as strings or regular expressions.

### Numeric Formats and Encodings

COBOL File Source supports conversion for numeric types including COMP, COMP-1, COMP-2, COMP-3, COMP-4, Zoned Decimal as well a wide range of encodings and character sets.

 

## Steps

Adding a COBOL file source object allows you to transfer data from a COBOL file. An example of what a COBOL file source object looks like is shown below.

![](cobol-file-source-images/mceclip0.png)

To configure the properties of a COBOL Source object after it was added to the dataflow, right-click on it and select Properties from the context menu.

<iframe width="560" height="315" src="https://www.youtube.com/embed/wkEVahIdU9U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

