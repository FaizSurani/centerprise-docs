# XML/JSON File Source

Adding an XML/JSON file source object allows you to transfer data from an XML or JSON file. An example of what an XML/JSON file source object looks like is shown below.

![](xmljson-file-source-images/mceclip0.png)

To configure the properties of an XML/JSON Source object after it was added to the dataflow, right-click on it and select Properties from the context menu. The following properties are available:

General Properties screen:

File Path – specifies the location of the source XML or JSON file. Using UNC paths is recommended if running the dataflow on a server.

Note: To open the source file for editing in a new tab, click  ![image46.gif](xmljson-file-source-images/image46.gif)icon next to the File Path input, and select Edit File.

Schema File Path – specifies the location of the XSD file controlling the layout of the XML source file.

Note: Centerprise can generate a schema based on the content of the source XML file.  The data types will be assigned based on the source file’s content.

To generate the schema, click  ![image46.gif](xmljson-file-source-images/image46.gif) icon next to the Schema File Path input, and select Generate.

To edit an existing schema, click  ![image46.gif](xmljson-file-source-images/image46.gif) icon next to the Schema File Path input, and select Edit File. The schema will open for editing in a new tab.

Optional Record Filter Expression – allows you to enter an expression to selectively filter incoming records according to your criteria. You can use Expression Builder to help you create your filter expression. For more information on using Expression Builder, see Expression Builder.

Note: To ensure that your dataflow is runnable on a remote server, please avoid using local paths for the source. Using UNC paths is recommended. 