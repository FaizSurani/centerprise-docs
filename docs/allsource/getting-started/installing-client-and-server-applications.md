# Installing Client and Server Applications


In this section we will discuss how to install and configure Centerprise Server and Lean Client applications.

You can download the installation package from here.

## How to install Centerprise Server

1. Run ‘IntegrationServer.exe’ from the installation package to start the server installation setup.

2. You’ll be directed to the welcome screen. Click Next to continue.

   ![](installing-client-and-server-applications-images/mceclip0.png)

3\. On the next screen you will see the license agreement. You can only continue if you choose to accept the terms of the license agreement. Click *Next* to continue.

![](installing-client-and-server-applications-images/mceclip1.png)

4\. On the next screen, enter the user details and click *Next* to continue.

![](installing-client-and-server-applications-images/mceclip2.png)

5\. You can specify/change the folder for installing Centerprise server on the next screen. By default, it is installed in **Program Files > Astera Software > Astera Integration Server 8****.** Click *Next* to continue.

![](installing-client-and-server-applications-images/mceclip3.png)

6\. Select the type of installation (Complete or Custom) you want to proceed with and click *Next*.

![](installing-client-and-server-applications-images/mceclip4.png)

7\. Select *Install* to complete the installation.

![](installing-client-and-server-applications-images/mceclip5.png)

8\. Select *Finish* to finish the installation process.

![](installing-client-and-server-applications-images/mceclip6.png)

## How to install Centerprise Lean Client

1. Run the *‘CenterpriseDataIntegrator’* application from the installation package to start the client installation setup.
2. You’ll be directed to the welcome screen. Click *Next* to continue.

![](installing-client-and-server-applications-images/mceclip0-1563481867367.png)

3\. On the next screen you will see the license agreement. You can only continue if you choose to accept the terms of the license agreement. Click *Next* to continue.

![](installing-client-and-server-applications-images/mceclip1-1563481871453.png)

4\. On the next screen, enter the user details and click *Next* to continue.

![](installing-client-and-server-applications-images/mceclip2-1563481879803.png)

5\. You can specify/change the folder for installing Centerprise server on the next screen. By default, it is installed in **Program Files > Astera Software > Centerprise Data Integrator 8 (Beta).** Click Next to continue.

![](installing-client-and-server-applications-images/mceclip3-1563481905891.png)

6\. Select the type of installation (Complete or Custom) you want to proceed with and click *Next*.

![](installing-client-and-server-applications-images/mceclip4-1563481910145.png)

If you select custom installation, you can choose specific component(s) that you want to download.

![](installing-client-and-server-applications-images/mceclip5-1563481913698.png)

I want to install the complete package therefore, I’ll select *Complete* on the *Setup Type* screen and click *Next*.

7\. Select *Install* to complete the installation.

![](installing-client-and-server-applications-images/mceclip6-1563481917307.png)

8\. Select *Finish* to finish the installation process.

![](installing-client-and-server-applications-images/mceclip7.png)

This is how you install Centerprise client and server applications. The next step is to establish a connection between Centerprise client and server. See how to [connect to a Centerprise server using lean client.](https://astera.zendesk.com/hc/en-us/articles/360021120374-Connecting-to-a-Centerprise-Server-using-Lean-Client)