# How to Configure the Server

Once the cluster database has been set up, the next step is to link the server to this cluster database. To do this, right-click on the server node in the *Server Explorer*tree and select *Configure Server*.

 ![](how-to-configure-the-server-images/mceclip0.png)

The *Server Connection Properties* screen will appear. Click on the button next to *Cluster DB Info* to link the server to the cluster database.

![](how-to-configure-the-server-images/mceclip1.png)

Fill in the details for the cluster database you previously created. Test the connection, click *OK,* and then save it.

![](how-to-configure-the-server-images/mceclip2.png)

The server will now be configured. The server icon will have a green arrow.

![](how-to-configure-the-server-images/mceclip3.png)