# How to Build a Cluster Database and Create Repository

Before you start using the Centerprise server, a repository must be set up. Centerprise supports SQL Server and PostgreSQL for building cluster databases, which can then be used for maintaining the repository. The repository is where job logs, job queues, and schedules are kept. The cluster database repository can be set up in two ways:

1. Build a cluster database
2. Generate the SQL script for the database in Centerprise

To see these options, go to *Server* menu.

![](how-to-build-a-cluster-database-and-create-repository-images/mceclip0.png)

In either case, the first step is to point to the SQL Server or PostgreSQL instance where you want to build the repository and provide the credentials to establish the connection.

**Note:** Centerprise will not create the database itself, just the tables. A database will have to be created beforehand or an existing database can be used.  We recommend Centerprise to have its own database for this purpose.

## Building a Repository on SQL Server

1. Select *Build Cluster Database* from the *Server* menu and the following Database Connection screen will appear.
2. Select SQL Server from the Data Provider drop-down list and provide the credentials for establishing the connection.
3. From the drop-down list next to the Database option, select the database on the SQL instance where you want to host the repository.

![](how-to-build-a-cluster-database-and-create-repository-images/mceclip1.png)

4\. Click *Test Connection* to test whether the connection is successfully established or not. You should be able to see the following message if the connection is successfully established.

![](how-to-build-a-cluster-database-and-create-repository-images/mceclip2.png)

## Building a Repository on PostgreSQL

1. Select *Build Cluster Database* from the *Server* menu and the following Database Connection screen will appear.
2. Select PostgreSQL from the Data Provider drop-down list and provide the credentials for establishing the connection.
3. From the drop-down list next to the *Database* option, select the database on the PostgreSQL instance where you want to host the repository.

![](how-to-build-a-cluster-database-and-create-repository-images/mceclip3.png)

4\. Click *Test Connection* to test whether the connection is successfully established or not. You should be able to see the following message if the connection is successfully established.

![](how-to-build-a-cluster-database-and-create-repository-images/mceclip4.png)

This is how you build a cluster database to maintain repository. There is one more step before you start using Centerprise server – configuring the server so that it points to the correct repository.