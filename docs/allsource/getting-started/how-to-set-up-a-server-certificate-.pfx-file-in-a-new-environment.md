# How to Set up a Server Certificate (.pfx) File in a New Environment

1\. To install an SSL certificate, go to **Program Files > Astera Software > Astera Integration Server 8** and replace the **certificate (.pfx)** file placed in the installation directory with the (.pfx) file provided to you by your SSL certificate provider.

2\. After you have replaced the file, open the **certificatesettings.json** file placed in the installation directory and update the new filename and password. The password will be provided to you by the SSL certificate provider.

![](how-to-set-up-a-server-certificate-.pfx-file-in-a-new-environment-images/mceclip0.png)

3\. Save the changes and reboot the services for ***Astera Integration Server 8*** and the Server certificate will be implemented.