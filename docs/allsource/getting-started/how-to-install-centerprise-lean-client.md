# How to Install Centerprise Lean Client

1. Run the *‘CenterpriseDataIntegrator’* application from the installation package to start the client installation setup.
2. You’ll be directed to the welcome screen. Click *Next* to continue.
	
	![](how-to-install-centerprise-lean-client-images/mceclip0.png)
	
3. On the next screen you will see the license agreement. You can only continue if you choose to accept the terms of the license agreement. Click *Next* to continue.
	
	![](how-to-install-centerprise-lean-client-images/mceclip1.png)
	
4. On the next screen, enter the user details and click *Next* to continue.
	
	![](how-to-install-centerprise-lean-client-images/mceclip2.png)
	
5. You can specify/change the folder for installing Centerprise server on the next screen. By default, it is installed in **Program Files > Astera Software > Centerprise Data Integrator 8 (Beta).** Click Next to continue.
	
	![](how-to-install-centerprise-lean-client-images/mceclip3.png)
	
6\. Select the type of installation (Complete or Custom) you want to proceed with and click *Next*.

![](how-to-install-centerprise-lean-client-images/mceclip4.png)

If you select custom installation, you can choose the specific component that you want to download.

![](how-to-install-centerprise-lean-client-images/mceclip5.png)

I want to install the complete package therefore, I’ll select *Complete* on the *Setup Type* screen and click *Next*.

7\. Select *Install* to complete the installation.

![](how-to-install-centerprise-lean-client-images/mceclip6.png)

8\. Select *Finish* to finish the installation process.

![](how-to-install-centerprise-lean-client-images/mceclip7.png)