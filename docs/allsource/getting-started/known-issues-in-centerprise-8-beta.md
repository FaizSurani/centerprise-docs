# Known Issues in Centerprise 8 (Beta)

Centerprise 8 (Beta) comes with significant changes in the product design architecture. To get more information about the features included in Centerprise 8, [refer to the release notes](https://astera.zendesk.com/knowledge/articles/360021308913/en-us?brand_id=825506).

However, we are aware of certain issues and limitations in Centerprise 8. These issues are either due to pending fixes or due to third-party software limitations. 

Here is the list of known issues:

1. Name and Address parser features are not available in Centerprise 8 (Beta). Functions not supported in beta release are:
   - ParseAddress
   - ParseAddressUS
   - ParseAndCorrectAddress
   - ParseName
   - ParseNameEx
2. Oracle bulk load is not supported until Oracle releases .Net Core update for the driver. You can use array insert as an alternative, which provides a similar performance benefit as the bulk load. If you need record level logging, you can use Single insert.
3. Spaces in field names will cause verification and run-time error. You will need to replace space with a different character, for example '_', as a workaround for this issue.
4. Legacy DDTek providers support is deprecated. Oracle - Legacy, DB2 - Legacy and Sybase -Legacy providers are not supported in version 8.  You are advised to use Oracle ODP and IBM DB2 drivers instead.
5. Lean client can only connect to one server at a time. This is done using the Server Connection screen at the client startup.  If the user needs to connect to a different server, they will need to close and restart the client, and then connect to the new server using the Server Connection screen.  To see the server currently connected to, go to ***View > Server Explorer***. The server will show under DEFAULT cluster. 
6. Load-balancing can be achieved using clusters of servers with a single repository database. There is no change in this functionality compared to version 7.5. However, only the currently connected server will be listed in Server Explorer, instead of all the servers in the cluster.
7. SOAP requests are not supported in Web Server Transformation object.