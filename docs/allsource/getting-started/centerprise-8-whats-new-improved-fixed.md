# Centerprise Data Integrator 8 – What’s New, Improved, and Fixed

With Centerprise 8.0, we have introduced a completely new client-server design implementation. Here's somethign else

Here’s a roundup of everything that’s new, improved, and fixed in the new generation of the Centerprise product line to give users a better, easier, and faster data integration experience:

## What’s New?

- **REST server design:** We are now shipping a REST-based Centerprise server with the release of version 8.0. We have transitioned from .NET remoting to RESTful web services.

- **Lean Centerprise client:** You asked for an easier way to deploy Centerprise across a company network, and we delivered! This is good news for your IT administrators. We have introduced a lightweight version of the Centerprise client with the 8.0 upgrade. Assemblies and DLLs (Dynamic Link Libraries) for most database drivers now reside on the Centerprise server instead of the client machines. Now you can scale horizontally and add more clients to an existing cluster of servers with minimum IT overhead.

- *Astera REST web service APIs:* 

  We have published an extensive library of Astera REST web service APIs. Now you can perform a wide range of data integration functions by using APIs to directly communicate with the Centerprise server and get the required data. Here are the resources that are available for the users in the first round of the beta release:

  - *Account APIs:* Resources related to account login and user authentication
  - *Server APIs:* Resources related to job monitoring, schedules, and server information.

- **Open source options for creating database repositories:** We have added the support for [building repository database on PostgreSQL](https://astera.zendesk.com/hc/en-us/articles/360021309733-How-to-Build-a-Cluster-Database-and-Create-Repository), in addition to the SQL Server.

## What’s Improved?

- **Improvements in List Lookup and DB Lookup transformations:** New options added to the layout builder screen of [List lookup](https://astera.zendesk.com/hc/en-us/articles/360020947233-List-Lookup-Transformation) and DB lookup transformations. Now you can keep the source value for the values that do not match the replacing criteria in a list or a database table.
- **Path Mappings tab added:** A new tab for specifying client and server path mappings has been added on the *Cluster Settings* This is particularly useful when the client and server reside on different networks or network file systems and their paths have to be mapped to correctly transfer files at run-time.
- **Option added to repair file paths:** For better project management, we have added a new option to repair the file paths for files in any particular folder. This option is useful if you are working in a case-sensitive environment such as Linux.
- **Shortcut for replacing parameters information for consolidated parameters added:** Now you can replace the common parameter information for all objects in a flow with this command right from the dataflow screen.
- **Pushdown mode is expanded and improved****:** More transformations and expressions are now supported in the Pushdown mode. We have also improved the explanations for the verification errors for easier troubleshooting.
- **Trace grid in the Job Progress window is improved:** A new command for finding the next error in the trace grid is added in the Job Progress window. Now you can easily navigate between errors in the job trace and look at the details.
- **Server and Client diagnostic files:** We have also added the option for generating client and server diagnostic files for better error logging and easier troubleshooting.
- **Salesforce UPSERT is fully supported:** Salesforce UPSERT is now fully supported. UPSERT uses an external ID field in the Salesforce table to match incoming data with existing records.
- **Keyboard shortcuts added and displayed:** Keyboard shortcuts are enabled for various task windows. You can view the shortcut keys next to their respective commands.
- **Performance improvements when mapping complex trees:** Runtime performance of the dataflows containing complex tree structures is improved.
- **New additions to the data providers list:** We have added Tableau to the list of supported data providers.
- **SCD write strategy improved:** SCD type 3 and type 6 are now supported.
- **REST call retries:** For the REST calls running in timeout error, Centerprise will automatically make up to three retry attempts to process the call.

Have questions or want to suggest any feature improvements? Let us know in the comments section below.

<!--stackedit_data:
eyJwcm9wZXJ0aWVzIjoidGl0bGU6ICdDZW50ZXJwcmlzZSBEYX
RhIEludGVncmF0b3IgOCAtIFdoYXQnJ3MgTmV3LCBJbXByb3Zl
ZCwgYW5kIEZpeGVkJ1xuYXV0aG9yOiBBbW1hcmEgQW5pc1xuc3
RhdHVzOiBwdWJsaXNoZWRcbmRhdGU6ICcyMDE5LTAzLTIwJ1xu
IiwiZGlzY3Vzc2lvbnMiOnsiYXhPNTFZN0g4dTBaWkd1SCI6ey
J0ZXh0IjoiV2l0aCBDZW50ZXJwcmlzZSA4LjAsIHdlIGhhdmUg
aW50cm9kdWNlZCBhIGNvbXBsZXRlbHkgbmV3IGNsaWVudC1zZX
J2ZXIgZGVzaWdu4oCmIiwic3RhcnQiOjY3LCJlbmQiOjE2MX0s
IjllNHVjMUxBb1VQMVRXanMiOnsidGV4dCI6IldoYXTigJlzIE
ltcHJvdmVkPyIsInN0YXJ0IjoxOTcyLCJlbmQiOjE5ODh9LCJh
bWJLejl1OGowcVF0UjdXIjp7InN0YXJ0IjoxNzIsImVuZCI6MT
kzLCJ0ZXh0Ijoicm91bmR1cCBvZiBldmVyeXRoaW5nIn19LCJj
b21tZW50cyI6eyJRaExhTWtIQlBHSnlWc1p0Ijp7ImRpc2N1c3
Npb25JZCI6ImF4TzUxWTdIOHUwWlpHdUgiLCJzdWIiOiJnbDpo
dHRwczovL2dpdGxhYi5jb20vNDMxMjc5MSIsInRleHQiOiJIZX
JlJ3MgYSBjb21tZW50ISIsImNyZWF0ZWQiOjE1NjMzMzU2NDQz
MzJ9LCJ1SmxOenlxTk51WjU4ZVlQIjp7ImRpc2N1c3Npb25JZC
I6IjllNHVjMUxBb1VQMVRXanMiLCJzdWIiOiJnbDpodHRwczov
L2dpdGxhYi5jb20vNDMxMjc5MSIsInRleHQiOiJIZXJlJ3MgYW
5vdGhlciBjb21tZW50ISIsImNyZWF0ZWQiOjE1NjMzMzU2NzE2
ODR9LCJWRTRGcENmN1VKVDdvRGhNIjp7ImRpc2N1c3Npb25JZC
I6ImFtYkt6OXU4ajBxUXRSN1ciLCJzdWIiOiJnbDpodHRwczov
L2dpdGxhYi5jb20vNDMxMjc5MSIsInRleHQiOiJGaXggdGhpcy
IsImNyZWF0ZWQiOjE1NjMzNDI1NzUxMDd9fSwiaGlzdG9yeSI6
Wy02MTMxNzg5OTcsMTAyMTYzNjE4NiwtNzAwMDk5MDg4LC00OD
I5NzE0MzAsLTcwMDA5OTA4OF19
-->