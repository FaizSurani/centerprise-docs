# Connecting to a Centerprise Server using Lean Client

## How to connect to a Centerprise Server from the Client Startup Screen

After you have successfully installed Centerprise client and server applications, open the client application and you will see the following *Server Connection* screen on startup.

 ![](connecting-to-a-centerprise-server-using-lean-client-images/mceclip0.png)

Enter the *Server URI* and *Port Number* to establish the connection.

The server URI should start with HTTPS:// as shown in the screenshot above. The default port for the secure connection between the Lean client and the Centerprise server is 9260.

If you have connected to any server recently, you can automatically connect to that server by selecting that server from the *Recently Used* drop-down list.

Click *Connect* after you have filled out the information required.

The client will now connect to the selected server. You should be able to see the server listed in the *Server Explorer* tree when the client application opens.

To open *Server Explorer* go to **Server > Server Explorer** or use the keyboard shortcut **(Ctrl + Alt + E)**.

 ![](connecting-to-a-centerprise-server-using-lean-client-images/mceclip1.png)

 

## How to automatically reconnect on Client Startup

If you don’t want Centerprise to show you the server connection screen every time you run the client application, you can skip that by modifying the settings.

To do that go to **Tools > Options > Client Startup** and select the *Auto Connect to Server* option. On enabling the option, Centerprise will store the server details you entered previously and will use those details to automatically reconnect to the server every time you run the application.

![](connecting-to-a-centerprise-server-using-lean-client-images/mceclip2.png)

![](connecting-to-a-centerprise-server-using-lean-client-images/mceclip3.png

