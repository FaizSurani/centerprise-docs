# How to Connect to a Different Centerprise Server from the Lean Client

You can connect to different servers right from the Server Explorer window in Lean Client. Go to the *Server Explorer* window and click on the Connect to Server icon.

 ![](how-to-connect-to-a-different-centerprise-server-from-the-lean-client-images/mceclip1.png)

A prompt will appear that will confirm if you want to disconnect from the current Server and establish connection to a different server. Click *Yes* to proceed.

**Note:** A client cannot be connected to multiple servers at once.

![](how-to-connect-to-a-different-centerprise-server-from-the-lean-client-images/mceclip2.png)

You will be directed to the *Server Connection* screen. Enter the required server information (Server URI and Port Number) to connect to the server and click *Connect*.

![](how-to-connect-to-a-different-centerprise-server-from-the-lean-client-images/mceclip3.png)

If the connection is successfully established, you should be able to see the connected server in the Server Explorer window.

![](how-to-connect-to-a-different-centerprise-server-from-the-lean-client-images/mceclip4.png)